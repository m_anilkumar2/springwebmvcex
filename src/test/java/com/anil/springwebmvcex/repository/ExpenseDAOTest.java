package com.anil.springwebmvcex.repository;

import com.anil.springwebmvcex.config.WebConfig;
import com.anil.springwebmvcex.model.Expense;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by anil on 7/16/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class})
@WebAppConfiguration
@Transactional
public class ExpenseDAOTest {

    @Autowired
    ExpenseDAO expenseDAO;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getExpensesByDate() throws Exception {
        Expense expense = getExpense();
        expenseDAO.save(expense);
        List<Expense> expenses = expenseDAO.getExpensesByDate(new Date());
        Assert.assertEquals(1,expenses.size());
    }
    @Test
    public void testSave(){
        Expense expense = getExpense();
        expenseDAO.save(expense);
    }


    @Test
    public void testGetAllExpenses(){
        Expense expense1 = getExpense();
        Expense expense2 = getExpense();
        expenseDAO.save(expense1);
        expenseDAO.save(expense2);
        Assert.assertEquals(2,expenseDAO.getAllExpenses().size());
    }

    private Expense getExpense() {
        Expense expense = new Expense();
        expense.setDescription("Test Expense1 Description");
        expense.setExpenseAmount(new BigDecimal(1000));
        expense.setExpenseName("Test Expense1");
        expense.setExpenseDate(new Date());
        return expense;
    }

}