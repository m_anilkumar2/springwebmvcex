package com.anil.springwebmvcex.controller;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import static org.junit.Assert.*;

/**
 * Created by anil on 7/16/2017.
 */
public class ExpenseControllerTest {

    private MockMvc mockMvc;
    @Before
    public void setup() {

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ExpenseController()).build();

    }
    @Test
    public void showAddExpenseForm() throws Exception {
        mockMvc.perform(get("/showAddExpense"))
                .andExpect(status().isOk())
                .andExpect(view().name("addExpense"));

    }

    @Test
    public void addExpense() throws Exception {
        mockMvc.perform(post("/createExpense"))
                .andExpect(status().isOk());
    }

}