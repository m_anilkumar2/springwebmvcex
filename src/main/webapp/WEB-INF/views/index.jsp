<%--
  Created by IntelliJ IDEA.
  User: anil
  Date: 7/9/2017
  Time: 10:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Index Page</title>
    <link href="<c:url value="/resources/css/master.css" />" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="header">
        <p>Expense Tracker</p>
    </div>
    <ul>
        <li><a href="home">Home</a></li>
        <li><a href="about">About</a></li>
        <li><a href="showAddExpense">Add Expense</a></li>
    </ul>
</body>
</html>