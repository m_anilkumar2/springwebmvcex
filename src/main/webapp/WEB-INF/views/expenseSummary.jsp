<%--
  Created by IntelliJ IDEA.
  User: anil
  Date: 7/30/2017
  Time: 10:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Expense Summary</title>
    <link href="<c:url value="/resources/css/master.css" />" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="form-header"><p>Expense Summary</p></div>
    <div class="expense-list">
        Number of expenses : ${allExpenses.size()}
    </div>
        <c:forEach items="${allExpenses}" var="expense">
            <div class="expense-list">
                <p class="expense-item">
                Expense id: ${expense.id}
                Expense Date: ${expense.expenseDate}
                Expense Name: ${expense.expenseName}
                Expense Amount: ${expense.expenseAmount}
                Expense Description: ${expense.description}
                </p>
             </div>
         </c:forEach>
</body>
</html>
