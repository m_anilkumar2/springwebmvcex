<%--
  Created by IntelliJ IDEA.
  User: anil
  Date: 7/16/2017
  Time: 5:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add Expense</title>
    <link href="<c:url value="/resources/css/master.css" />" rel="stylesheet" type="text/css" />
</head>
<body>

<div class="form-header">
    <p>Add Expense </p>
</div>
<div class="form-div">
    <form:form action="createExpense" modelAttribute="expense">


            <div>
                Expense Date
                <form:input type="text" path="expenseDate"/>
            </div>
            <div>
                Expense Name
                <form:input type="text" path="expenseName"/>
            </div>
            <div>
                Expense Amount
                <form:input type="text" path="expenseAmount"/>
            </div>
            <div>
                Expense Description
                <form:input type="text" path="description"/>
            </div>
            <div>
                <input type="submit" value="Add Expense"/>
            </div>
    </form:form>
</div>
</body>
</html>
