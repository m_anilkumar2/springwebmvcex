package com.anil.springwebmvcex.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by anil on 8/27/2017.
 */
@ControllerAdvice
public class GenericExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception e){
        ModelAndView modelAndView =new ModelAndView("error/genericError");
        modelAndView.addObject("name",e.getClass().getSimpleName());
        modelAndView.addObject("errorMsg",e.getMessage());
        return  modelAndView;
    }
}
