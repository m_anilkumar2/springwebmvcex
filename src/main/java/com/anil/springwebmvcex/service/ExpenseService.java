package com.anil.springwebmvcex.service;

import com.anil.springwebmvcex.model.DailyExpense;
import com.anil.springwebmvcex.model.Expense;
import com.anil.springwebmvcex.repository.ExpenseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by anil on 7/16/2017.
 */

@Service
public class ExpenseService {

    @Autowired
    ExpenseDAO expenseDAO;

    public DailyExpense getDailyExpenses(){
        return  null;
    }

    public List<Expense> getMonthlyExpenses(){
        return  null;
    }

    @Transactional
    public void saveExpense(Expense expense){
        expenseDAO.save(expense);
    }

    @Transactional
    public List<Expense> getAllExpenses(){
        return  expenseDAO.getAllExpenses();
    }
}
