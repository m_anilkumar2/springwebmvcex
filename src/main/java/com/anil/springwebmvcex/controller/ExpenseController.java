package com.anil.springwebmvcex.controller;

import com.anil.springwebmvcex.model.Expense;
import com.anil.springwebmvcex.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by anil on 7/16/2017.
 */
@Controller
public class ExpenseController {

    @Autowired
    private  ExpenseService expenseService;

    @RequestMapping(value = "/showAddExpense",method = RequestMethod.GET)
    public String showAddExpenseForm(Model model){
        System.out.println("in showAddExpenseForm");
        Expense expense = new Expense();
        model.addAttribute("expense",expense);
        return "addExpense";
    }

    @RequestMapping(value = "/createExpense",method =RequestMethod.POST )
    public String addExpense(@ModelAttribute("expense")Expense expense, BindingResult bindingResult ,ModelMap model){
        System.out.println("in addExpense");
        expenseService.saveExpense(expense);
        List<Expense> allExpenses = expenseService.getAllExpenses();
        System.out.println("all expenses "+allExpenses.size());
        model.addAttribute("allExpenses",allExpenses);
        return "expenseSummary";
    }

    /*Notes:
    Title: Example of @Excpetion Handler
    Disadvantage:Exception Handler per Controller, so configure globally using other methods
    By default it shows status code 200, if you want to send error status code
    use @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    we can jsps in web.xml based on error codes.
    <error-code>
    </error-code>
    we can use jsp way of defining error page
    <error-page>
        <location>/WEB-INF/views/jsp/error.jsp</location>
    </error-page>
    for json excpetion we can annotate @ExceptionHandler
     */
//    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)

//    @ExceptionHandler(Exception.class)
//    public ModelAndView handleException(Exception ex){
//
//        ModelAndView model = new ModelAndView();
//        model.addObject("errorMsg",ex.getMessage());
//        model.setViewName("error/genericError");
//        return  model;
//    }
}
