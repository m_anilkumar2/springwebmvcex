package com.anil.springwebmvcex.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by anil on 7/9/2017.
 */
@Controller
public class HelloController {

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String index(ModelMap modelMap){
        modelMap.addAttribute("message","Spring MVC Java Configuration");
        return "index";
    }

}
