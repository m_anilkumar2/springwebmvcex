package com.anil.springwebmvcex.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by anil on 7/16/2017.
 */
@Repository
public class BaseDAO<T,PK> {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public void save(T entity){
        getSession().save(entity);
    }
}
