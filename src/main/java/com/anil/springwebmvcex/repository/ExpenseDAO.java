package com.anil.springwebmvcex.repository;

import com.anil.springwebmvcex.model.Expense;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by anil on 7/16/2017.
 */

@Repository
public class ExpenseDAO extends BaseDAO<Expense,Long>{


    public List<Expense> getExpensesByDate(Date date){
        Query query = getSession().createQuery("from Expense e where e.expenseDate=:date");
        query.setParameter("date",date);
        return query.list();
    }

    public List<Expense> getAllExpenses(){
        Query query = getSession().createQuery("from Expense e ");
        return query.list();
    }

}
